CREATE TABLE user (
  `id` INT AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `role` CHAR(9) NOT NULL CHECK(`role` IN ('moderator', 'publisher')),
  `password` CHAR(60) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE article (
	`id` INT AUTO_INCREMENT,
	`title` VARCHAR(50) NOT NULL,
	`publication_date` DATE DEFAULT CURRENT_DATE,
	`content` TEXT NOT NULL,
	`user` INT NOT NULL,
	PRIMARY KEY (`id`),
  FOREIGN KEY (`user`) REFERENCES `user`(`id`)
);

CREATE TABLE opinion (
  `positive` BOOLEAN NOT NULL,
  `article` INT NOT NULL,
  `user` INT NOT NULL,
	PRIMARY KEY (`article`, `user`),
  FOREIGN KEY (`article`) REFERENCES `article`(`id`) ON DELETE CASCADE,
  FOREIGN KEY (`user`) REFERENCES `user`(`id`) ON DELETE CASCADE
);
