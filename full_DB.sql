-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 23, 2023 at 10:34 AM
-- Server version: 10.6.12-MariaDB
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `articles`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `publication_date` date DEFAULT curdate(),
  `content` text NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `title`, `publication_date`, `content`, `user`) VALUES
(1, 'The Art of Meditation', '2022-03-15', 'Meditation is a powerful tool for managing stress and improving overall wellbeing. In this article, we explore the art of meditation and how it can benefit your mind and body.', 1),
(2, 'The Science of Sleep', '2022-03-14', 'Sleep is an essential component of a healthy lifestyle. In this article, we delve into the science of sleep and explore how it affects your body, mind, and overall wellbeing.', 2),
(3, 'The Benefits of Yoga', '2022-03-13', 'Yoga is a popular form of exercise that has many physical and mental health benefits. In this article, we explore the benefits of yoga and how it can improve your overall wellbeing.', 3),
(4, 'How to Start a Successful Business', '2022-03-12', 'Starting a successful business requires careful planning, hard work, and dedication. In this article, we provide tips and advice on how to start a successful business.', 4),
(5, 'The Importance of Nutrition', '2022-03-11', 'Nutrition plays a crucial role in maintaining a healthy lifestyle. In this article, we explore the importance of nutrition and how it can improve your overall health and wellbeing.', 5),
(6, 'The Power of Positive Thinking', '2022-03-10', 'Positive thinking can have a significant impact on your life and overall wellbeing. In this article, we explore the power of positive thinking and how it can improve your mental and physical health.', 6),
(7, '10 Tips for Better Sleep', '2022-01-01', 'Lorem ipsum dolor sit amet...', 1),
(8, 'The Benefits of Meditation', '2022-01-02', 'Lorem ipsum dolor sit amet...', 2),
(9, 'The Best Restaurants in Town', '2022-01-03', 'Lorem ipsum dolor sit amet...', 3),
(10, 'How to Start a Business from Scratch', '2022-01-04', 'Lorem ipsum dolor sit amet...', 4),
(11, 'The Importance of Exercise', '2022-01-05', 'Lorem ipsum dolor sit amet...', 5),
(12, '10 Easy Recipes for Busy Weekdays', '2022-01-06', 'Lorem ipsum dolor sit amet...', 6),
(13, 'Traveling on a Budget: Tips and Tricks', '2022-01-07', 'Lorem ipsum dolor sit amet...', 7),
(14, 'The Future of AI and Robotics', '2022-01-08', 'Lorem ipsum dolor sit amet...', 8),
(15, 'The Latest Trends in Fashion', '2022-01-09', 'Lorem ipsum dolor sit amet...', 9),
(16, '10 Ways to Reduce Stress', '2022-01-10', 'Lorem ipsum dolor sit amet...', 10),
(17, 'How to Improve Your Productivity at Work', '2022-01-11', 'Lorem ipsum dolor sit amet...', 1),
(18, 'The Impact of Climate Change', '2022-01-12', 'Lorem ipsum dolor sit amet...', 2),
(19, '10 Must-See Tourist Attractions', '2022-01-13', 'Lorem ipsum dolor sit amet...', 3),
(20, 'The Benefits of Volunteering', '2022-01-14', 'Lorem ipsum dolor sit amet...', 4),
(21, 'The Most Popular Sports in the World', '2022-01-15', 'Lorem ipsum dolor sit amet...', 5),
(22, '10 Tips for Successful Online Dating', '2022-01-16', 'Lorem ipsum dolor sit amet...', 6),
(23, 'The Latest Developments in Technology', '2022-01-17', 'Lorem ipsum dolor sit amet...', 7),
(24, 'The Most Influential People in History', '2022-01-18', 'Lorem ipsum dolor sit amet...', 8),
(25, 'The Benefits of a Healthy Diet', '2022-01-19', 'Lorem ipsum dolor sit amet...', 9),
(26, '10 Ways to Improve Your Memory', '2022-01-20', 'Lorem ipsum dolor sit amet...', 10);

-- --------------------------------------------------------

--
-- Table structure for table `opinion`
--

CREATE TABLE `opinion` (
  `positive` tinyint(1) NOT NULL,
  `article` int(11) NOT NULL,
  `user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `opinion`
--

INSERT INTO `opinion` (`positive`, `article`, `user`) VALUES
(0, 1, 1),
(1, 1, 2),
(0, 1, 4),
(1, 1, 5),
(0, 1, 7),
(1, 1, 8),
(0, 2, 2),
(1, 2, 3),
(0, 2, 5),
(1, 2, 6),
(0, 2, 8),
(1, 2, 9),
(1, 3, 1),
(0, 3, 3),
(1, 3, 4),
(0, 3, 6),
(1, 3, 7),
(0, 3, 9),
(0, 4, 1),
(1, 4, 2),
(0, 4, 4),
(1, 4, 5),
(0, 4, 7),
(1, 4, 8),
(0, 5, 2),
(1, 5, 3),
(0, 5, 5),
(1, 5, 6),
(0, 5, 8),
(1, 5, 9),
(1, 6, 1),
(0, 6, 3),
(1, 6, 4),
(0, 6, 6),
(1, 6, 7),
(0, 6, 9),
(0, 7, 1),
(1, 7, 2),
(0, 7, 4),
(1, 7, 5),
(1, 7, 8),
(1, 8, 3),
(1, 8, 6),
(1, 8, 9),
(1, 9, 1),
(1, 9, 4),
(1, 9, 7),
(1, 10, 2),
(1, 10, 5),
(1, 10, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `role` char(9) NOT NULL CHECK (`role` in ('moderator','publisher')),
  `password` char(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `role`, `password`) VALUES
(1, 'john_doe', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(2, 'jane_doe', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(3, 'jim_smith', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(4, 'jack_brown', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(5, 'jessica_jones', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(6, 'peter_parker', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(7, 'bruce_wayne', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(8, 'clark_kent', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(9, 'tony_stark', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
(10, 'steve_rogers', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `opinion`
--
ALTER TABLE `opinion`
  ADD PRIMARY KEY (`article`,`user`),
  ADD KEY `user` (`user`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`);

--
-- Constraints for table `opinion`
--
ALTER TABLE `opinion`
  ADD CONSTRAINT `opinion_ibfk_1` FOREIGN KEY (`article`) REFERENCES `article` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `opinion_ibfk_2` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
