INSERT INTO user (username, role, password)
VALUES
  ('john_doe', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jane_doe', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jim_smith', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jack_brown', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jessica_jones', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('peter_parker', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('bruce_wayne', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('clark_kent', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('tony_stark', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('steve_rogers', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6')
;
