DROP TABLE IF EXISTS opinion, article, user;
CREATE TABLE user (
  `id` INT AUTO_INCREMENT,
  `username` VARCHAR(50) NOT NULL,
  `role` CHAR(9) NOT NULL CHECK(`role` IN ('moderator', 'publisher')),
  `password` CHAR(60) NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE article (
	`id` INT AUTO_INCREMENT,
	`title` VARCHAR(50) NOT NULL,
	`publication_date` DATE DEFAULT CURRENT_DATE,
	`content` TEXT NOT NULL,
	`user` INT NOT NULL,
	PRIMARY KEY (`id`),
  FOREIGN KEY (`user`) REFERENCES `user`(`id`)
);

CREATE TABLE opinion (
  `positive` BOOLEAN NOT NULL,
  `article` INT NOT NULL,
  `user` INT NOT NULL,
	PRIMARY KEY (`article`, `user`),
  FOREIGN KEY (`article`) REFERENCES `article`(`id`) ON DELETE CASCADE,
  FOREIGN KEY (`user`) REFERENCES `user`(`id`) ON DELETE CASCADE
);
INSERT INTO user (username, role, password)
VALUES
  ('john_doe', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jane_doe', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jim_smith', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jack_brown', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('jessica_jones', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('peter_parker', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('bruce_wayne', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('clark_kent', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('tony_stark', 'publisher', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6'),
  ('steve_rogers', 'moderator', '$2y$10$Bg5u/eNG6dNTQ2j.qzf6auCy3RATa.8DTvVZPpIvKFtakT5EPI2a6')
;
INSERT INTO article (title, publication_date, content, user)
VALUES
  ('The Art of Meditation', '2022-03-15', 'Meditation is a powerful tool for managing stress and improving overall wellbeing. In this article, we explore the art of meditation and how it can benefit your mind and body.', 1),
  ('The Science of Sleep', '2022-03-14', 'Sleep is an essential component of a healthy lifestyle. In this article, we delve into the science of sleep and explore how it affects your body, mind, and overall wellbeing.', 2),
  ('The Benefits of Yoga', '2022-03-13', 'Yoga is a popular form of exercise that has many physical and mental health benefits. In this article, we explore the benefits of yoga and how it can improve your overall wellbeing.', 3),
  ('How to Start a Successful Business', '2022-03-12', 'Starting a successful business requires careful planning, hard work, and dedication. In this article, we provide tips and advice on how to start a successful business.', 4),
  ('The Importance of Nutrition', '2022-03-11', 'Nutrition plays a crucial role in maintaining a healthy lifestyle. In this article, we explore the importance of nutrition and how it can improve your overall health and wellbeing.', 5),
  ('The Power of Positive Thinking', '2022-03-10', 'Positive thinking can have a significant impact on your life and overall wellbeing. In this article, we explore the power of positive thinking and how it can improve your mental and physical health.', 6),
  ('10 Tips for Better Sleep', '2022-01-01', 'Lorem ipsum dolor sit amet...', 1),
  ('The Benefits of Meditation', '2022-01-02', 'Lorem ipsum dolor sit amet...', 2),
  ('The Best Restaurants in Town', '2022-01-03', 'Lorem ipsum dolor sit amet...', 3),
  ('How to Start a Business from Scratch', '2022-01-04', 'Lorem ipsum dolor sit amet...', 4),
  ('The Importance of Exercise', '2022-01-05', 'Lorem ipsum dolor sit amet...', 5),
  ('10 Easy Recipes for Busy Weekdays', '2022-01-06', 'Lorem ipsum dolor sit amet...', 6),
  ('Traveling on a Budget: Tips and Tricks', '2022-01-07', 'Lorem ipsum dolor sit amet...', 7),
  ('The Future of AI and Robotics', '2022-01-08', 'Lorem ipsum dolor sit amet...', 8),
  ('The Latest Trends in Fashion', '2022-01-09', 'Lorem ipsum dolor sit amet...', 9),
  ('10 Ways to Reduce Stress', '2022-01-10', 'Lorem ipsum dolor sit amet...', 10),
  ('How to Improve Your Productivity at Work', '2022-01-11', 'Lorem ipsum dolor sit amet...', 1),
  ('The Impact of Climate Change', '2022-01-12', 'Lorem ipsum dolor sit amet...', 2),
  ('10 Must-See Tourist Attractions', '2022-01-13', 'Lorem ipsum dolor sit amet...', 3),
  ('The Benefits of Volunteering', '2022-01-14', 'Lorem ipsum dolor sit amet...', 4),
  ('The Most Popular Sports in the World', '2022-01-15', 'Lorem ipsum dolor sit amet...', 5),
  ('10 Tips for Successful Online Dating', '2022-01-16', 'Lorem ipsum dolor sit amet...', 6),
  ('The Latest Developments in Technology', '2022-01-17', 'Lorem ipsum dolor sit amet...', 7),
  ('The Most Influential People in History', '2022-01-18', 'Lorem ipsum dolor sit amet...', 8),
  ('The Benefits of a Healthy Diet', '2022-01-19', 'Lorem ipsum dolor sit amet...', 9),
  ('10 Ways to Improve Your Memory', '2022-01-20', 'Lorem ipsum dolor sit amet...', 10)
;
INSERT INTO opinion (`positive`, `article`, `user`) VALUES
(TRUE, 1, 2),
(TRUE, 1, 5),
(TRUE, 1, 8),
(TRUE, 2, 3),
(TRUE, 2, 6),
(TRUE, 2, 9),
(TRUE, 3, 1),
(TRUE, 3, 4),
(TRUE, 3, 7),
(TRUE, 4, 2);
INSERT INTO opinion (`positive`, `article`, `user`) VALUES
(TRUE, 4, 5),
(TRUE, 4, 8),
(TRUE, 5, 3),
(TRUE, 5, 6),
(TRUE, 5, 9),
(FALSE, 1, 1),
(FALSE, 1, 4),
(FALSE, 1, 7),
(FALSE, 2, 2),
(FALSE, 2, 5);
INSERT INTO opinion (`positive`, `article`, `user`) VALUES
(FALSE, 2, 8),
(FALSE, 3, 3),
(FALSE, 3, 6),
(FALSE, 3, 9),
(FALSE, 4, 1),
(FALSE, 4, 4),
(FALSE, 4, 7),
(FALSE, 5, 2),
(FALSE, 5, 5),
(FALSE, 5, 8);
INSERT INTO opinion (`positive`, `article`, `user`) VALUES
(TRUE, 6, 1),
(TRUE, 6, 4),
(TRUE, 6, 7),
(TRUE, 7, 2),
(TRUE, 7, 5),
(TRUE, 7, 8),
(TRUE, 8, 3),
(TRUE, 8, 6),
(TRUE, 8, 9),
(TRUE, 9, 1);
INSERT INTO opinion (`positive`, `article`, `user`) VALUES
(TRUE, 9, 4),
(TRUE, 9, 7),
(TRUE, 10, 2),
(TRUE, 10, 5),
(TRUE, 10, 8),
(FALSE, 6, 3),
(FALSE, 6, 6),
(FALSE, 6, 9),
(FALSE, 7, 1),
(FALSE, 7, 4);
