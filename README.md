# Database

The database scheme & sample test data.

Intended to be used with MariaDB (MySQL).

## Full database export

the full_db.sql file contains the whole database, including the scheme and some sample data described below.

### Sample users

(id, username, role)

1. john_doe : publisher
1. jane_doe : moderator
1. jim_smith : publisher
1. jack_brown : moderator
1. jessica_jones : publisher
1. peter_parker : moderator
1. bruce_wayne : publisher
1. clarck_kent : moderator
1. tony_stark : publisher
1. steve_rogers : moderator
