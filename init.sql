CREATE USER 'articles'@'localhost' IDENTIFIED BY 'password';
CREATE DATABASE IF NOT EXISTS articles
CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON articles.*TO 'articles'@'localhost';
FLUSH PRIVILEGES;
